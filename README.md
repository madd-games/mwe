# Madd Windows Environment

MWE is a _package manager_ with the intention of creating a development environment under Windows, and to allow releases of Madd tools.

## Installation

Installer for Windows: http://share.madd-games.org/mwe/mwe-setup.exe

MWE gets installed under `C:\mbsroot`, and this directory becomes the Windows equivalent of `/usr`. The `bin` and `lib` directories are added
 to the `PATH` by the installer. After installation, you should be able to open the command line and run:

```
mwe fetch-index
```

...to fetch up-to-date package indexes.

## Installing POSIX commands and GCC

You can run:

```
mwe install mingw
```

To install GCC as well as `coreutils` (which is a dependency of MinGW and thus gets installed with it). This can take a while!

## Upgrades

Run `mwe upgrade` to upgrade all installed packages to their latest versions.